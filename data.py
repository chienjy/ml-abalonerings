"""
project: abaloneRings
creater: 11920
date:    2021-12-21
info:    NONE
"""
import pandas as pd
import numpy as np
from torch.utils.data import Dataset
import random
from utils import attribute2numeric
from sklearn.preprocessing import scale

split_num = 3000


class Data4Tree:
    def __init__(self, f_path=None, train=True, shuffle=False):
        if f_path is None:
            f = pd.read_csv('abalone.csv', header=None)
        else:
            f = pd.read_csv(f_path, header=None)

        f.columns = ['Sex', 'Length', 'Diameter', 'Height', 'Whole_w', 'Shucked_w', 'Viscera_w', 'Shell_w', 'Rings']

        # 将Rings划分
        for i in range(len(f)):
            # print(f.loc[i, 'Rings'])
            if f.loc[i, 'Rings'] in range(0, 8):
                f.loc[i, 'Rings'] = 1
            elif f.loc[i, 'Rings'] in range(8, 10):
                f.loc[i, 'Rings'] = 2
            else:
                f.loc[i, 'Rings'] = 3

        if shuffle:
            f.sample(frac=1).reset_index(drop=True)

        # 数值化
        f = pd.get_dummies(f)

        if train:
            f = f[:split_num]
        else:
            f = f[split_num:]

        self.feature_names = f.columns.tolist()
        self.feature_names.remove('Rings')
        self.target_names = []
        for i in range(1, 30):
            self.target_names.append(str(i))

        self.target = f['Rings'].tolist()
        f = f.drop(['Rings'], axis=1)
        # f = scale(f)
        # print(f)
        self.data = f.values.tolist()

    def load_data4tree(self):
        # 返回一个字典
        result = {'data': [], 'target': []}

        for i in range(len(self.data)):
            result['data'].append(self.data[i])
        for i in range(len(self.target)):
            result['target'].append(self.target[i])
        result['data'] = np.array(result['data'])
        result['target'] = np.array(result['target'])
        return result


class Data4logistics:
    def __init__(self, path=None, train=True, sc=True, split=True, dum=True):
        if path is None:
            path = 'abalone.csv'
        self.f = pd.read_csv(path, header=None)
        self.f.columns = ['Sex', 'Length', 'Diameter', 'Height', 'Whole_w', 'Shucked_w', 'Viscera_w', 'Shell_w', 'Rings']

        # 数值化
        if dum:
            self.f = pd.get_dummies(self.f)

        if split:
            if train:
                self.f = self.f[:split_num]
            else:
                self.f = self.f[split_num:]

        self.feature = ['Sex', 'Length', 'Diameter', 'Height', 'Whole_w', 'Shucked_w', 'Viscera_w', 'Shell_w', 'Rings']

        # 具体根据target所在的列决定
        if dum:
            target_num = -4
        else:
            target_num = -1
        self.y = self.f[self.f.columns[target_num]]
        self.x = self.f.drop([self.f.columns[target_num]], axis=1)
        s = self.feature[target_num]
        # print(s)
        self.feature.remove(s)

        # 归一化
        if sc:
            self.x = scale(self.x)


if __name__ == '__main__':
    data = Data4logistics()
    # data = Data4Tree(shuffle=False)
    # print(data.target)
    print(data.y.head())

    print(data.f.head())
    print(data.f.columns.tolist().remove('Rings'))
    # print(data.load_data4tree())
    # print(data.target_names)


