"""
project: abaloneRings
creater: 11920
date:    2021-12-21
info:    NONE
"""
from libsvm import *
from libsvm.svmutil import *
from libsvm.svm import *
import graphviz

from sklearn import linear_model
from sklearn import tree
from sklearn.metrics import accuracy_score, roc_curve

from data import Data4logistics, Data4Tree
from utils import loss_LR

import os

from visual import fun1, plot_confusion_matrix, plot_roc, plot_importance

os.environ["PATH"] += os.pathsep + 'D:/Graphviz/bin/'


def train_decision_tree():
    """
    使用决策树三分类
    """
    # model = tree.DecisionTreeRegressor()
    model = tree.DecisionTreeClassifier(min_samples_split=2,
                                        min_samples_leaf=10,
                                        criterion='entropy',
                                        splitter='random')
    dataset_train = Data4Tree(train=True)
    dataset_test = Data4Tree(train=False)
    model.fit(dataset_train.data, dataset_train.target)

    pred = model.predict(dataset_test.data)
    # print(pred)
    print('accuracy: ', accuracy_score(dataset_test.target, pred))
    # 绘制混淆矩阵
    plot_confusion_matrix(dataset_test.target, pred)
    # print(loss_LR(dataset_test.target, pred))

    # 导出决策树PDF文件
    dot_data = tree.export_graphviz(model,
                                    out_file=None,
                                    feature_names=dataset_train.feature_names,
                                    class_names=dataset_train.target_names,
                                    filled=True, rounded=True,
                                    special_characters=True)
    import pydotplus
    graph = pydotplus.graph_from_dot_data(dot_data)
    graph.write_pdf('tree_clf.pdf')


def train_regression():
    """
    使用线性回归训练数据
    """
    # model = linear_model.LogisticRegression(penalty='l2', solver='lbfgs', class_weight=None)
    model = linear_model.LinearRegression()
    dataset_train = Data4logistics(train=True, sc=False)
    dataset_test = Data4logistics(train=False, sc=False)

    model.fit(dataset_train.x, dataset_train.y)
    pred = model.predict(dataset_test.x)
    # print('模型验证准确率：', accuracy_score(dataset_test.y, pred))
    # print(pred)
    print(loss_LR(dataset_test.y.tolist(), pred))

    # 可视化
    show_num = 20
    fun1(pred[:show_num], dataset_test.y.tolist()[:show_num])

    # 特征权重
    feature_importance = model.coef_.ravel()
    t_l = dataset_test.f.columns.tolist()
    t_l.remove('Rings')
    feature_names = t_l
    plot_importance(feature_importance, feature_names)


if __name__ == '__main__':
    # 原始数值：            (total: 7344, loss: 6.239592183517417)
    # 归一化：             (total: 3382, loss: 2.873406966864911)
    # 不考虑样本的分布权重：  (total: 1842, loss: 1.5649957519116398)
    # 线性回归：            (total: 1857.1762053480381, loss: 1.5778897241699559)
    train_regression()

    # 决策树回归：0.19
    # (2512.0, 2.1360544217687076)
    # 决策树29分类：0.22
    # (2347, 1.995748299319728)
    # 决策树三分类：0.62 min_leaf=1
    #            0.66 min_leaf=5
    #            0.68 min_leaf=10 entropy
    train_decision_tree()

