"""
project: abaloneRings
creater: 11920
date:    2021-12-23
info:    NONE
"""
import pandas as pd
import numpy as np


def str2num():
    data = pd.read_csv('abalone.csv', header=None)
    print(data.head())
    data[0] = data[0].apply(lambda x: -1 if x == 'I' else 0 if x == 'M' else 1)
    print(data.head())
    data.to_csv('abalone.numerical.csv', header=None, index=None)


def csv2svm():
    data = pd.read_csv('abalone.numerical.csv', header=None)
    file = open('abalone.svm.txt', 'w', newline='')

    for i in range(len(data)):
        index = 1
        newline = str(data.loc[i][8])
        for j in range(8):
            newline = newline + ' ' + str(index) + ':' + str(data.loc[i][j])
            index = index + 1
        file.write(newline + '\n')
    file.close()


def decimal2onehot(len, value):
    """
    将十进制数值转为one-hot编码

    :param len: length of domain of value
    :param value: value begins at 0
    :return: result: array of one-hot code
    """
    if value >= len:
        return None
    result = np.zeros(len, dtype=int)
    result[value] = 1
    return result


def attribute2numeric(sourceDF, val_names):
    """
    将数据集中的名义变量转为数值变量，使用one-hot转换

    :param sourceDF: source dataframe of pandas csv
    :param val_names: attribute names
    :type sourceDF: Dataframe
    :type val_names: list
    :return: dataframe
    """
    # 复制
    df = pd.DataFrame()

    for val in val_names:
        grp = sourceDF.groupby(val)
        n = grp.size()[0]
        # 删除对应属性

        # 添加修改后的属性，不能在最后一列添加

    return df


def loss_LR(target, pred):
    """
    逻辑回归的损失函数
    :param target: label
    :param pred: pred
    :type target: list
    :type pred: list
    :return: loss
    """
    n = len(target)
    total_loss = 0
    for i in range(n):
        loss = abs(target[i] - pred[i])
        total_loss = total_loss + loss

    return total_loss, total_loss/n


if __name__ == '__main__':
    # csv2svm()
    length = 15
    a = 14
    print(decimal2onehot(len, a))

