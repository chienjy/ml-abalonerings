"""
project: abaloneRings
creater: 11920
date:    2021-12-21
info:    提供可视化方法
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sb
from sklearn.metrics import confusion_matrix, roc_curve, auc

from data import Data4logistics


def show_bar(data, column_name, cnt):
    """
    为数据绘制柱状图，查看分布情况

    :param data: 数据集中某个属性的列表
    :param label: default=None, describe of data
    :type data: list
    :type label: list
    """
    plt.hist(data, bins=np.arange(cnt+1)-0.5, rwidth=0.5)
    plt.title(column_name)
    plt.show()


def show_box(data):
    """
    为数据绘制箱线图，查看分布情况

    :param data: 数据集中某个属性的列表
    :type data: list
    """


def show_scatter(x, y, feature_name):
    plt.scatter(x, y)
    plt.title(feature_name)
    plt.show()


def attribute_distribute():
    f = pd.read_csv('abalone.csv', header=None)
    f.columns = ['Sex', 'Length', 'Diameter', 'Height', 'Whole weight', 'Shucked weight', 'Viscera weight', 'Shell weight', 'Rings']
    print(f.shape[1])
    for i in range(f.shape[1]):
        col_name = f.columns[i]
        show_bar(f[col_name].tolist(), col_name, len(f[col_name].value_counts()))
        # print(f[i].tolist())


def fun1(pred, label):
    # print(pred)
    # print(label)
    for i in range(len(pred)):
        x_p = i+1
        x_t = i+1
        y_p = pred[i]
        y_t = label[i]
        plt.scatter(x_p, y_p, color='b')
        plt.scatter(x_t, y_t, color='r')
        plt.plot([x_p, x_t], [y_p, y_t], color='k')
    plt.xlabel('Samples')
    plt.ylabel('Rings')
    plt.xticks(range(0, len(pred)+2))
    plt.show()


def plot_confusion_matrix(y, y_pred):
    plt.title('confusion matrix', y=-0.1)
    sb.heatmap(confusion_matrix(y, y_pred), cmap='PuBu', annot=True, fmt='g', cbar=False)
    plt.xlabel(f'predict class')
    plt.xticks((0.5, 1.5, 2.5), ('1-8', '9-10', '>11'))
    plt.ylabel('actual class')
    plt.yticks((0.5, 1.5, 2.5), ('1-8', '9-10', '>11'))
    ax = plt.gca()
    ax.xaxis.set_label_position('top')
    ax.xaxis.set_ticks_position('top')
    plt.tight_layout()
    plt.show()


def plot_roc(y, y_pred_proba):
    fpr, tpr, thresholds = roc_curve(y, y_pred_proba)
    plt.title('ROC curve')
    plt.text(0.2, 0.6, f'AUC = {round(auc(fpr, tpr), 4)}', fontsize=15)
    plt.plot(fpr, tpr)
    plt.plot((0, 1), (0, 1), linestyle='--')
    plt.tight_layout()
    plt.show()


def plot_importance(importance_list, feature_names):
    print(importance_list)
    print(feature_names)
    plt.title('feature importance')
    sb.barplot(x=importance_list, y=feature_names, palette=['#2077B4'])
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    # arr = [1.5, 0.6, 7.8, 6]
    # attribute_distribute(arr)
    # attribute_distribute()
    # data = pd.read_csv('abalone.csv', header=None)
    # data.columns = ['Sex', 'Length', 'Diameter', 'Height', 'Whole_w', 'Shucked_w', 'Viscera_w', 'Shell_w', 'Rings']
    # data = pd.get_dummies(data)
    # print(data)
    d = Data4logistics(sc=False, split=False, dum=False)
    for item in d.feature:
        show_scatter(d.x[item], d.y, item)



